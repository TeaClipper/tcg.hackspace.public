//              .;iLGGG00G00GGGCCCLLf1:
//            ,iG000000000GGCGGGGGCCGGCGGt.
//          .t0000000000000GCCGGCCCCCGG0000Gt.
//         1000GCLffffCCGGGCGGGGGCLLfii11fG0GCLi
//        ;GGCCLLfftttLCCGGGGGCGCLLft:;;;;iLCGGGC,
//       :GGGCLLffft11tffLCGCCLLfftti:::::;;1CCGGG,
//      ,C00CLLLfftt1ii;iLGGCfii11i;:,::::::;ifCG00:
//     ,G000CCCLfftt11i;;:::::;;;:,,,,,::::;;iifCG0C.
//     t0000GCCLLftt1ii;;;;;::::::,,,::::::;;ii1LCG0L
//    ,00000GGCCfft11ii;;;:::::::,,,,:::::;;;iiiLG00f
//    100000GGCCLtt11ii;;;:;:::::,,,:,,::::;;;i1C000f
//    f00000GGGCLfftt1ii;;;;;;;;:::::::;ii;;;ii1G080G.
//     t00000000GGGCGGGGCf1i;;;;;1tLGGGCLLLtiiitG0800t
//     ,L080G0000GGGCLLCGGCLt1;;itttfffffttff1if08800,
//      t080GGCCG0CLGGLiLfG0C1;;ttffCGCCLfft1iiL0000t
//     .t000GCLffttttt111itLfi:;;:;1iff;iii;;;iCLt0:
//      .CGGGCLft11iiiiii1tft;:;;::;;ii;;;;;;i1C1,.
//       LGG0GLft11iii;i1tfLt;;i;;::::;;;;;;;i1t,
//      .iCG0GCLft11iiiitCCf1::;;;;:,,::;;;;i111;.
//       iCG0GGCLftt1iitCfCf1:;;iii;::::;;iii11f1,
//       ,fG000GCLft111L000Gfii1ti:;i;;;;;ii1ttCCC,
//        :10000GCLfttfLCG00GCtiiii;;;iiiii1t1f0GC1.
//        .tG000GCCLLCGGCCLtitf1111111111tt1tfGGGC,
//         100000GCffffLCCLfLfffft11t11i111tfCCCC,
//         .G0000GGCLLffffLLLLLt1iii111tttffG0;,
//          .GGG000GCCLft1iiii;;iiiii1ttfLt;88G,.
//            f0000000GCft1111iii111fLCCt,L888888800f,
//           i0000000000GCffffffffLGCt;,i88888888888808
//          .C0800000000000GGGCfti;,.. L888888888880888
//          :LC800G000GGGG00fi;:.    ;08888888888888888
//      .tG0ftL0800GG0GCLC1:,.     .L888888888888888888
//.1G008888f0Lt0880Ci;iifGC.    .,108888888888888888888
//888888880f0C:G88G,.,.,:L0L ..,;G088888888888888880C0L
//888888888C08ii881...  .t00,,;C0088888888888888GL88880
//888888888808G,tC.     ,t0C:100880888888888GG0GG080888

/**
	@file		main.cpp
	@author		Copyright © 2014 Ed Reid, Tea Clipper Games Ltd. All rights reserved.
 */

#include <stdio.h>
#include <SDL/SDL.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

void UpdateFrame();
SDL_Surface *screen;

static const int SURFACE_WIDTH = 720;
static const int SURFACE_HEIGHT = 405;

int main(int argc, char** argv) {
    printf("what what\n");
    SDL_Init(SDL_INIT_VIDEO);
    screen = SDL_SetVideoMode(SURFACE_WIDTH, SURFACE_HEIGHT, 32, SDL_SWSURFACE);
    
#ifdef TEST_SDL_LOCK_OPTS
    EM_ASM("SDL.defaults.copyOnLock = false; SDL.defaults.discardOnLock = true; SDL.defaults.opaqueFrontBuffer = false;");
#endif

#ifdef __EMSCRIPTEN__
    printf("emscripten_set_main_loop!\n");

    emscripten_set_main_loop(UpdateFrame, 60, 1);
#else
    while (1) {
        UpdateFrame();
        // Delay to keep frame rate constant (using SDL)
        SDL_Delay(time_to_next_frame());
    }
#endif

    printf( "SDL QUIT!!!\n");
    SDL_Quit();
    
    return 0;
}

void UpdateFrame()
{
    static int chaos = 0;
    chaos++;
    chaos = chaos % (255 * 2);
    
    if (SDL_MUSTLOCK(screen)) SDL_LockSurface(screen);
    
    for (int i = 0; i < SURFACE_HEIGHT; i++) {
        for (int j = 0; j < SURFACE_WIDTH; j++) {
#ifdef TEST_SDL_LOCK_OPTS
            // Alpha behaves like in the browser, so write proper opaque pixels.
            int alpha = 255;
#else
            // To emulate native behavior with blitting to screen, alpha component is ignored.
            // Test that it is so by outputting data (and testing that it does get discarded)
            int alpha = (i+j) % 255;
#endif
            int r = ((i+chaos) % (255 * 2)) >= 255 ? (255 - (i+chaos) %255) : (i+chaos) %255;
            int g = ((j+chaos) % (255 * 2)) >= 255 ? (255 - (j+chaos) %255) : (j+chaos) %255;
            int b = (255 * 2)-chaos >= 255? 255-chaos : (255 + chaos) % 255;
            *((Uint32*)screen->pixels + i * SURFACE_WIDTH + j) = SDL_MapRGBA(screen->format, r, g, b, alpha);
        }
    }

    if (SDL_MUSTLOCK(screen)) SDL_UnlockSurface(screen);
    SDL_Flip(screen);
}