var gulp = require('gulp');
var gutil = require('gulp-util');
var fs = require('fs');
var proc = require('child_process');

var cppContent = ['src/**/*.cpp', 'src/**/*.h'];
var webContent = ['src/**/*.html', 'src/**/*.js', 'src/**/*.css'];

gulp.task('default', ['emscripten', 'copyweb']);

gulp.task('watch', ['emscripten', 'copyweb'], function () {
    gulp.watch(cppContent, ['emscripten']);
    gulp.watch(webContent, ['copyweb']);

    var express = require('express');
    var livereload = require('express-livereload');
    var app = express();

    livereload(app,  { watchDir: 'bin/html5' });

    app.use(express.static('src'));
    app.use(express.static('bin/html5'));

    var server = app.listen(3000, function () {

        var host = server.address().address;
        var port = server.address().port;

        console.log('Listening at http://%s:%s', host, port)

    })
});

gulp.task('copyweb', function () {
    return gulp.src(webContent).pipe(gulp.dest('bin/html5'));
});

gulp.task('emscripten', function (callback) {
    fs.readFile('./src/build.json', function (err, data) {
        if (err) {
            gutil.log('emscripten', err);
        }

        var config = JSON.parse(data);
        var errorLog = null;

        var compiler = proc.spawn('em++', config.files.concat(['-o', config.output], config.arguments));
        compiler.stderr.on('data', function (data) {
            errorLog += data;
        });

        compiler.on('close', function (code) {
            if (errorLog) {
                gutil.log('emscripten - \n' + errorLog);
            } else if (code !== 0) {
                gutil.log('emscripten - Error code ' + code);
            }

            callback();
        })
    })
});