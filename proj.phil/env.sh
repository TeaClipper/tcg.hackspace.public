#!/bin/bash

pushd ./tools/emsdk_portable
./emsdk construct_env
source ./emsdk_set_env.sh
popd

PATH="./node_modules/.bin:${PATH}"
export PATH