#!/bin/bash

# Base file structure
mkdir ./bin
mkdir ./bin/html5
mkdir ./bin/native

# Setup NPM and build system
npm install

# Setup Emscripten
pushd ./tools/emsdk_portable/

./emsdk update
./emsdk install latest
./emsdk activate latest

popd