# tldr
The phil toolchain: `clone` > `sh setup.sh` > `source ./env.sh` > `gulp watch` > `write C++` > `save` > `./bin/html5/main.html`

# Initial setup
Run `./setup.sh`

This will install the components for the build system, setup an initial folder structure, and install Emscripten

# Shell setup
For everything to work properly, you must ensure that the local environment is setup with the proper paths.
You can do this in two different ways

## Shell-local
Execute `source ./env.sh` to do this. You'll need to do this for every new terminal window you create.

## Globally
You can also add this command so it's executed every time you open any new command prompt.

Add `source <path_to_root>/env.sh` to the ~/.bash_profile file in your user root folder.

**N.B.: If you've installed Emscripten before, remove it and allow the version included in the repository to be setup and
configured instead.**

# Building

## Input files
The list of input source files lives in **src/build.json**

* Add to the `files` array to add more input files.
* Add to the `arguments` array to configure the arguments passed along to the em++ compiler.

## Compiling
* `gulp` - To execute the compiler once
* `gulp watch` - To start the compiler and leave it continuously watching for changes. This will also start a web server, see Applying section for more details.

Errors will be reported to this console window.

To stop gulp watching files press `CTRL + C` in the console window.

# Applying
Connect to http://localhost:3000/main.html
For livereload to work, install your browser's 'livereload' plugin
Whenever you save a source file, it will be picked up by the build system, and then automatically reloaded in the browser.