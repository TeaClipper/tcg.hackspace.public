# Tea Clipper Games Ltd hackspace readme

This repository contains experimental projects and useful utilities.

Please feel free to use these tools as they are provided under the MIT license.